from datetime import datetime as dt
from datetime import timedelta
import pandas as pd
import requests
import gspread
from gspread_formatting.dataframe import format_with_dataframe
from gspread_dataframe import set_with_dataframe
## Define color codes
class Colors:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'
# Print the decoded string in a specific color
class Formitize:
    def __init__(self, api_token, ):
        self.headers = {"Authorization": f"Bearer {api_token}"}
        self.retry = 0
        self.max_retry = 3
        self.FIXED_COLUMNS = None
        self.forms = None
        self.pairs = dict()
        self.worksheet_names = []
    def get_json_response(self, url, params=None):
        """
        Method to get api response from a specific endpoint
        """
        try:
            if params:
                response = requests.get(url, headers=self.headers,params=params)
            else:
                response = requests.get(url, headers=self.headers)
            self.retry = 0
            return response.json()
        except:
            if self.retry == self.max_retry:
                return
            else:
                self.retry += 1
                print(f'Retry Time number : {self.retry}')
                self.get_json_response(url)

    def get_fixed_columns(self, json_data):
        """
        Method to get the fixed column like
        location, Persona que realiza el monitoreo etc.

        input : json_data(dict/json)
        return : dataframe of fixed columns and its values
        """
        # removed
        # 'version','jobID','count', 'content',
        cols = ['submittedFormID','formID','userID', 'userName','title',
        'formTitle','formDateCreated', 'dateSubmitted', 'dateModified',
        'formDataLastSaved', 'modifiedBy','latitude','longitude','location',
                'status']
        data = {i:json_data[i] for i in cols}
        try:
            url = json_data['attachments']['0']['url']
            data['pdf_link'] = url
        except:
            data['pdf_link'] = None
        #pd.DataFrame(data.values(), columns=data.keys())
        self.FIXED_COLUMNS = data

    def get_flexible_columns(self, json_data):
        """
        Method to get the fixed column like
        location, Persona que realiza el monitoreo etc.

        input : json_data(dict/json)
        return : dataframe of fixed columns and its values
        """
        #
        additional_data = {}
        children = json_data['content']
        for key in children.keys():
            if 'children' in list(children[key].keys()):
                children_dict = dict()
                for child in children[key]['children'].keys():
                    for child_key in children[key]['children'][child]:
                        name = children[key]['children'][child][child_key]['name']
                        value = children[key]['children'][child][child_key]['value']
                        value_type = children[key]['children'][child][child_key]['type']
                        if name not in children_dict.keys():
                            if value_type == 'formMultiple':
                                children_dict[name] = [next(iter(value.values()))]
                            else:
                                children_dict[name] = [value]
                        else:
                            if value_type == 'formMultiple':
                                children_dict[name].append(next(iter(value.values())))
                            else:
                                children_dict[name].append(value)
            elif isinstance(children[key], dict):
                value_type = children[key]['type']
                if value_type == 'formMultiple':
                    value = list(children[key]['value'].values())
                    if len(value) == 1:
                        value = value[0]
                    name = children[key]['name']
                elif 'image' in children[key].keys():
                    value = children[key]['image']
                    name = children[key]['name']
                elif 'images' in children[key].keys():
                    value = list(children[key]['images'].values())
                    if len(value) == 1:
                        value = value[0]
                    name = children[key]['name']
                elif value_type == 'pdf':
                    name = 'pdf'
                else:
                    value = children[key]['value']
                    name = children[key]['name']
                additional_data[name] = [value]
            else:
                additional_data[key] = children[key]
        fixed = self.get_fixed_columns(json_data)
        addition = pd.DataFrame(additional_data)
        children = pd.DataFrame(children_dict)
        columns = list(fixed.columns) + list(addition.columns)\
                                       + list(children.columns)
        addition = addition.applymap(str)
        fixed = fixed.applymap(str)
        children[addition.columns] = list(addition.values[0])
        children[fixed.columns] = list(fixed.values[0])

        return children[columns]


    def get_all_data(self):
        """
        Method to organize the content ...
        """
        main_df = self.value_pairs_to_df()
        for index,key in enumerate(self.FIXED_COLUMNS):
            value = self.FIXED_COLUMNS[key]
            main_df.insert(index, key, value)
        date_cols = ['formDateCreated','dateSubmitted',
                     'dateModified','formDataLastSaved']
        fn = lambda x : dt.fromtimestamp(int(x)).strftime('%Y-%m-%d %H:%M:%S')
        main_df[date_cols] = main_df[date_cols].applymap(fn)
        main_df = main_df.applymap(self.remove_list_with_one_value)
        main_df = main_df.sort_values(by='dateModified',ascending=False).reset_index(drop=True)
        self.pairs = dict()
        return main_df

    def get_forms_info(self):
        """
        Method to get all the form ids
        """
        url = 'https://service.formitize.com/api/rest/v2/form/list'
        if self.forms:
            return self.forms
        json_forms = self.get_json_response(url)
        forms_ids = list(json_forms['payload'].keys())
        forms = {i:json_forms['payload'][i]['title'] for i in forms_ids}
        self.forms = forms
        return self.forms

    def get_full_report_by_url(self, url):
        """
        Method to get all the full reports from it's url

        """
        list_reports = []
        page = 1
        while True:
            params = {'page':page}
            json_report = self.get_json_response(url=url, params=params)
            sub_report = pd.DataFrame(json_report['payload'])
            list_reports.append(sub_report)
            if sub_report.shape[0] < 500:
                break
            else:
                page += 1
        full_report = pd.concat(list_reports, ignore_index=True)
        return full_report

    def value_pairs_to_df(self):
        """
        Method to transform the json data into dataframe
        input : json_data(dict/json)
        return : dataframe
        """
        column_lists = dict()
        for key,value in self.pairs.items():
            if isinstance(value, list):
                if len(value) == 1:
                    value = value[0]
                else:
                    column_lists[key] = len(value)
        count_list_values = list(column_lists.values())
        if len(set(count_list_values)) > 1:
            to_explod = []
            max_len = max(count_list_values)
            for key in column_lists:
                value = column_lists[key]
                if value == max_len:
                    to_explod.append(key)
        else:
            to_explod = list(column_lists.keys())
        df_pairs = pd.json_normalize(self.pairs)
        if to_explod:
            df_pairs = df_pairs.explode(to_explod)
        return df_pairs

    def get_obj_name(self, obj):
        """
        Method to get the value of key name of a dict object:
        Example :
        obj = {'name':'Jhon','value':7}
        name = get_obj_name(obj)
        print(name)
        >>> 'Jhon'  
        """
        return obj['name']

    def get_obj_value(self, obj, key='value', multiple=False):
        # value key is the key of the value default is 'value'
        # example obj = {'name':'jhon','pdf':'https://www.google.com'}
        # mutiple if for images to return
        # mutiple images in one single list
        try:
            if isinstance(obj[key], dict):
                if multiple:
                    value = list(obj[key].values())
                else:
                    if obj[key]:
                        value = next(iter(obj[key].values()))
                    else:
                        value = ''
            else:
                value = obj[key]
        except:
            print(obj)
            zz
        return value

    def get_submitted_froms_by_fromID(self, formID):
        submitted_ids = []
        page_nbr = 1
        while True:
            url = 'https://service.formitize.com/api/rest/v2/form/submit/list'
            params = {'page':page_nbr, 'formID':formID}
            json_response = self.get_json_response(url,params=params)
            if json_response['payload']:
                ids = list(json_response['payload'].keys())
                submitted_ids.extend(ids)
                page_nbr += 1
            else:
                break
        return submitted_ids

    def remove_list_with_one_value(self, x):
        if isinstance(x, list):
            if len(x) == 1:
                return x[0]
        return x

    def get_submitted_forms_by_id(self, list_submitted_ids):
        """
        Method to extract multiple submitted forms by id
        """
        if isinstance(list_submitted_ids, list):
            submitted_ids = ','.join(list_submitted_ids)
        url = f'https://service.formitize.com/api/rest/v2/form/submit/?id={submitted_ids}'

        json_response = self.get_json_response(url)
        if len(list_submitted_ids) == 1:
            json_submitted_ids = {submitted_ids:json_response['payload']}
        else:
            json_submitted_ids = json_response['payload']
        return json_submitted_ids
    def get_flexible_columns(self, obj):
        """
        Method to extract key and it's value from a give json
        """
        if isinstance(obj, dict):
            if 'type' in obj:
                obj_type = obj['type']
                # check the object type
                # all the types are available below if
                if obj_type in ['formText','formCRM']:

                    obj_name = self.get_obj_name(obj)
                    if obj_name not in self.pairs:

                        self.pairs[obj_name] = obj['value']
                    else:
                        print(obj_name, 'duplicated')
                        #self.pairs.extend((obj_name,obj['value']))
                elif obj_type == 'formPhoto':
                    # photos object
                    obj_name = self.get_obj_name(obj)
                    obj_value = self.get_obj_value(obj, key='images', multiple=True)
                    self.pairs[obj_name] = [obj_value]
                elif obj_type == 'formCRMLocation':
                    location_cols = ['address1', 'address2','city',
                                    'postcode','state','country']
                    for column in location_cols:
                        self.pairs[column] = obj[column]

                elif obj_type == 'formSubheader':
                    # signature object pdf
                    if obj['children']:
                        for key in obj['children']:
                            self.get_flexible_columns(obj['children'][key])

                elif obj_type == 'formRow' and obj['children']:
                    # type fromrow contain mutiple values
                    first_row_elem = next(iter(obj['children']))
                    row_columns = list(obj['children'][first_row_elem].keys())
                    for column in row_columns:
                        if column not in self.pairs:
                            self.pairs[column] = []
                    for row in obj['children']:
                        for column in row_columns:
                            if 'value' not in obj['children'][row][column]:
                                self.get_flexible_columns(obj['children'][row][column])
                            else:
                                value = self.get_obj_value(obj['children'][row][column])
                                self.pairs[column].append(value)

                elif obj_type == 'formMultiple':
                    obj_name = self.get_obj_name(obj)
                    obj_value = self.get_obj_value(obj)
                    self.pairs[obj_name] = obj_value

                elif obj_type == 'formSignature':
                    # signature object pdf
                    obj_name = self.get_obj_name(obj)
                    obj_value = self.get_obj_value(obj, 'image')
                    self.pairs[obj_name] = obj_value

            else:
                for value in obj.values():
                    if isinstance(value, dict):
                        self.get_flexible_columns(value)
        else:
            print(value)
            for value in obj.values():
                print
                if isinstance(value, dict):
                    self.get_flexible_columns(value)
    def update_sheet(self, spreadsheet, form_name, fromData):
        try:
            if form_name not in self.worksheet_names:
                spreadsheet.add_worksheet(form_name,rows=0,cols=0)
                self.worksheet_names.append(form_name)
            worksheet = spreadsheet.worksheet(form_name)
            worksheet.clear()
            set_with_dataframe(worksheet, fromData)
            format_with_dataframe(worksheet, fromData, include_column_header=True)
            print(Colors.GREEN + f'{form_name} Updated Successfully' + Colors.END)
            self.retry = 0
        except:
            if self.retry == self.max_retry:
                print(Colors.RED + f"Can't update : {form_name}" + Colors.END)
                self.retry = 0
            else:
                self.retry += 1
                print(f'Retry Time number : {self.retry}')
                time.sleep(0.5)
                self.update_sheet(spreadsheet, form_name, fromData)
    def get_formid_modified_after_date(self, date):
        """
        getting the submitted forms modified after a specific date
        input: date(str)
        return:
        """
        url = 'https://service.formitize.com/api/rest/v2/form/submit/list'
        submitted_ids = []
        page_nbr = 1
        while True:
            url = 'https://service.formitize.com/api/rest/v2/form/submit/list'
            params = params={'page':page_nbr,'modifiedAfterDate':date}
            json_response = self.get_json_response(url,params=params)
            if json_response['payload']:
                data = pd.DataFrame(json_response['payload'].values())\
                                 [['submittedFormID','formID']]
                submitted_ids.append(data)
                page_nbr += 1
            else:
                break
        df_submitted = pd.concat(submitted_ids,ignore_index=True)
        df_submitted = df_submitted.applymap(str)
        dict_submitted_ids = df_submitted.groupby('formID')['submittedFormID']\
                          .agg(list).to_dict()
        return dict_submitted_ids
    def get_ready_to_update_data(self, form_ids):
        """
        Final dataframe
        """
        df_formIDs = []
        # split froms into list of chunks of 200
        list_form_ids = [form_ids[i:i+200] for i in range(0,len(form_ids),200)]
        for form_id in list_form_ids:
            submitted_forms = self.get_submitted_forms_by_id(form_id)
            for formID in submitted_forms:
                submitted_formID = submitted_forms[formID]
                self.get_flexible_columns(submitted_formID)
                self.get_fixed_columns(submitted_formID)
                df_formID = self.get_all_data()
                # add the specific
                df_formIDs.append(df_formID)
        if df_formIDs:
            fromData = pd.concat(df_formIDs,ignore_index=True)
        else:
            fromData = pd.DataFrame()
        return fromData

google_creds = {}

credentialds = {}
if __name__ == '__main__':
    gc = gspread.service_account_from_dict(google_creds)
    # with open('formatize_api_gsheets.json') as f:
    #     credentialds = json.load(f)
    todays_date = dt.now()
    yesterday_date = todays_date - timedelta(days=1)
    formatted_yesterday_date = yesterday_date.strftime('%Y-%m-%d')
    print(Colors.DARKCYAN + f' Updating submitted froms after{formatted_yesterday_date}' + Colors.END)
    for api_formitize, sheet_id in credentialds.items():
        error_forms = []
        Formitize_object = Formitize(api_token=api_formitize)
        all_submitted_forms = dict()
        Formitize_object.get_forms_info()
        updated_forms = Formitize_object.get_formid_modified_after_date(formatted_yesterday_date)
        for fromID in updated_forms:
            form_name = Formitize_object.forms[fromID]
            submitted_forms = Formitize_object.get_submitted_froms_by_fromID(fromID)
            if len(submitted_forms):
                print(Colors.UNDERLINE + f'{form_name} has: {len(submitted_forms)} submitted froms' + Colors.END)
                all_submitted_forms[fromID] = submitted_forms
            else:
                print(Colors.BLUE + f'{form_name} is empty' + Colors.END)
        spreadsheet = gc.open_by_key(sheet_id)
        worksheet_list = spreadsheet.worksheets()
        Formitize_object.worksheet_names = [i.title for i in worksheet_list]
        for form in all_submitted_forms:
            form_name = Formitize_object.forms[form]
            print(Colors.BOLD + f'Processing from: {form_name}' + Colors.END)
            form_ids = all_submitted_forms[form]

            fromData = Formitize_object.get_ready_to_update_data(form_ids)
            form_name = Formitize_object.forms[form]
            error_sheet = Formitize_object.update_sheet(spreadsheet,form_name,fromData)
            if error_sheet:
              error_forms.append(error_sheet)
        # sheet error test
        for form in error_forms:
            form_name = Formitize_object.forms[form]
            print(Colors.BOLD + f'Processing from: {form_name}' + Colors.END)
            form_ids = all_submitted_forms[form]

            fromData = Formitize_object.get_ready_to_update_data(form_ids)
            form_name = Formitize_object.forms[form]
            Formitize_object.update_sheet(spreadsheet,form_name,fromData)

        list_reports = {
            'jobs':'https://service.formitize.com/api/rest/v2/job/',
            'invoices':'https://service.formitize.com/api/rest/v2/crm/accounts/invoice/list',
        }
        for report_name in list_reports:
            url = list_reports[report_name]
            report = Formitize_object.get_full_report_by_url(url)
            if report_name not in Formitize_object.worksheet_names:
                spreadsheet.add_worksheet(report_name,rows=0,cols=0)
            try:
                worksheet = spreadsheet.worksheet(report_name)
                worksheet.clear()
                set_with_dataframe(worksheet, report)
                format_with_dataframe(worksheet, report, include_column_header=True)
                print(report_name,'Updated Successfully')
            except:
                print(f"Can't update : {report_name} report")